showRecentTransactions(JSON.parse(localStorage.getItem("recentTransactions")));

function showRecentTransactions(data) {

  if (!data) return;

  const tableBody = document.querySelector("#recent tbody");
  let str = "";

  data.forEach((element, index) => {

    let statmessage = element.statusMessage.toLowerCase();
    let statMsg;

    switch (statmessage.toUpperCase()) {

      case "TRANSACTION SUCCESSFUL":
        statMsg = "successfulTr";
        break;

      case "SUCCESSFUL":
        statMsg = "successfulTr";
        break;

      case "FAILED":
        statMsg = "failedTr";
        break;

      case "PENDING":
        statMsg = "pendingTr";
        break;

      case "ERROR":
        statMsg = "errorTr";
        break;

      case "INSUFFICIENT FUND":
        statMsg = "failedTr";
        break;

      case "REFER TO CARD ISSUER":
        statMsg = "referToCrd";
        break;

      case "ISSUER OR SWITCH INOPERATIVE":
        statMsg = "issuerOrSwitchInOperative";
        break;

      case "PROCESSING":
        statMsg = "processingTr";
        break;

      case "TRANSACTION NOT DONE":
        statMsg = "transactionNtDone";
        break;

      case "ROUTING ERROR":
        statMsg = "failedTr";
        break;

      default:
        statMsg = "failedTr";
        break;
    }

    const bName = element.businessName.substr(0, 20);

    const businessName2 = bName.toUpperCase();
    document.getElementById('bName').innerHTML = businessName2;

    const rowValue =
      `<tr>
        <td id="bName" class="item-cell yapa">
            <span>${bName}</span>
        </td>

        <td id="transactionId" class="item-cell txt">
            <span>${element.transactionId}</span>
        </td>

        <td id="transactionType" class="item-cell cashout">
           <span>${element.transactionType}</span>
        </td>

        <td id="amount" class="item-cell amount">
            <span>${"₦" + element.amount.toLocaleString()}</span>
        </td>

        <td id="statusMessage" class="item-cell success ${statMsg}">
          <span>${element.statusMessage.toLocaleString()}</span>
        </td>

        <td id="transactionTime" class="item-cell time">
            <span>${element.transactionTime}</span>
        </td>
    </tr>`;
    str += rowValue;
 
  });
  tableBody.innerHTML = str;
}

$.getJSON('https://paycentre.paypad.com.ng/vr/transaction/recent', function (res) {

  const { data } = res;
  window.localStorage.setItem("recentTransactions", JSON.stringify(data));
  showRecentTransactions(data);
})