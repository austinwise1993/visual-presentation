
function loadAgentInfoFromLS() {
  totalAgent = localStorage.getItem("totalAgent");
  signedUpThisMonth = localStorage.getItem("signedUpThisMonth");
  signedUpLastMonth = localStorage.getItem("signedUpLastMonth");
  signedUpTwoMonthsAgo = localStorage.getItem("signedUpTwoMonthsAgo");
  signedUpToday = localStorage.getItem("signedUpToday");
  signedUpYesterday = localStorage.getItem("signedUpYesterday");
  signedUpTwoDaysAgo = localStorage.getItem("signedUpTwoDaysAgo");
  totalActiveAgent = localStorage.getItem("totalActiveAgent");
  totalInactiveAgent = localStorage.getItem("totalInactiveAgent");

  document.getElementById('totalAgent').innerHTML = totalAgent;
  document.getElementById('signedUpThisMonth').innerHTML = signedUpThisMonth;
  document.getElementById('signedUpLastMonth').innerHTML = signedUpLastMonth;
  document.getElementById('signedUpTwoMonthsAgo').innerHTML = signedUpTwoMonthsAgo;
  document.getElementById('signedUpToday').innerHTML = signedUpToday;
  document.getElementById('signedUpYesterday').innerHTML = signedUpYesterday;
  document.getElementById('signedUpTwoDaysAgo').innerHTML = signedUpTwoDaysAgo;
  document.getElementById('totalActiveAgent').innerHTML = totalActiveAgent;
  document.getElementById('totalInactiveAgent').innerHTML = totalInactiveAgent;
}

loadAgentInfoFromLS();

$.getJSON('https://paycentre.paypad.com.ng/vr/agent', function (data, set) {

  let {
    totalAgent,signedUpThisMonth,signedUpLastMonth,signedUpTwoMonthsAgo,signedUpToday,signedUpYesterday,signedUpTwoDaysAgo,totalActiveAgent, totalInactiveAgent
  } = data.data;

  localStorage.setItem("totalAgent", `${totalAgent.toLocaleString()}`);
  localStorage.setItem("signedUpThisMonth", `${signedUpThisMonth.toLocaleString()}`);
  localStorage.setItem("signedUpLastMonth", `${signedUpLastMonth.toLocaleString()}`);
  localStorage.setItem("signedUpTwoMonthsAgo", `${signedUpTwoMonthsAgo.toLocaleString()}`);
  localStorage.setItem("signedUpToday", `${signedUpToday.toLocaleString()}`);
  localStorage.setItem("signedUpYesterday", `${signedUpYesterday.toLocaleString()}`);
  localStorage.setItem("signedUpTwoDaysAgo", `${signedUpTwoDaysAgo.toLocaleString()}`);
  localStorage.setItem("totalActiveAgent", `${totalActiveAgent.toLocaleString()}`);
  localStorage.setItem("totalInactiveAgent", `${totalInactiveAgent.toLocaleString()}`);

  loadAgentInfoFromLS();

});


showTopPerformingAgents(JSON.parse(localStorage.getItem("topAgentsPerformance")));

function showTopPerformingAgents(topPerformingAgents) {

  if(!topPerformingAgents) return;

  const tableBody = document.querySelector("#xxx tbody");
  let str = "";

  topPerformingAgents.forEach((element, index) => {
    
    const businessName = element.businessName.substr(0, 20);
    const businessName2 = element.businessName.toUpperCase();
    document.getElementById('businessName').innerHTML = businessName2;
      
    const rowValue =
      `<tr>
        <td class="item-cell eds cell1"><span id="volume">${index+1}</span></td>
        <td class="item-cell eds cell2">
          <span id="businessName" class="businessName">${businessName}</span>
        </td>
        <td class="item-cell eds item-cell-up  cImg" id"myImage">
          <img src="images/arrow-up.svg" alt="Smiley face" height="42" width="42">
        </td>
        <td class="item-cell eds cell3">
          <span id="value">${element.volume.toLocaleString()}</span>
        </td>
      </tr>`;
    str += rowValue;  
  });
  tableBody.innerHTML = str;

}

showTopPerformingStates(JSON.parse(localStorage.getItem("topStatesPerformance")));

function showTopPerformingStates(topPerformingStates) {

  if(!topPerformingStates) return;

  const tableBody2 = document.querySelector("#xyz tbody");
  let str2 = "";

  topPerformingStates.forEach((element, index) => {

    let state = element.state.toUpperCase();

    const agentValue =
      `<tr>
        <td class="item-cell eds cell1 ">
          <span id="volume">${index+1}</span></td>
        <td class="item-cell eds cell2">
          <span id="states">${state}</span>
        </td>
        <td class="item-cell eds item-cell-up cImg">
          <img src="images/arrow-up.svg" alt="Smiley face" height="42" width="42">
        </td>
        <td class="item-cell eds cell3">
          <span id="value">${element.volume.toLocaleString()}</span>
        </td>
      </tr>`;
    str2 += agentValue;
  });
  tableBody2.innerHTML = str2;

}

$.getJSON('https://paycentre.paypad.com.ng/vr/performance', function (data) {

  const {topPerformingAgents, topPerformingStates} = data.data;
  
  window.localStorage.setItem("topAgentsPerformance", JSON.stringify(topPerformingAgents));
  showTopPerformingAgents(topPerformingAgents);

  window.localStorage.setItem("topStatesPerformance", JSON.stringify(topPerformingStates));
  showTopPerformingStates(topPerformingStates);

})
