showGraphs(JSON.parse(localStorage.getItem("graphData")));

function showGraphs(data) {
  if (!data) return;
  showValueChart(data);
  showVolumeChart(data);
}

$.getJSON("https://paycentre.paypad.com.ng/vr/transaction/charts", {}, function (data) {
  
  localStorage.setItem("graphData", JSON.stringify(data));
  showGraphs(data);
  
});

function showVolumeChart(data) {
 
  const dataPoints = [];

  function addData(data) {

    for (var i = 0; i < data.data.volume.length; i++) {
      let date = data.data.volume[i][1].split(" ");
      // console.log(date)
      let vol = data.data.volume[i][0];
      // console.log(new Date(date[0]))
      dataPoints.push({
        x: new Date(date[0]),
        y: vol
      });
    }
    chart.render();
  };
  // console.log(dataPoints)

  const chart = new CanvasJS.Chart("chartContainer", {
    animationEnabled: true,
    theme: "light1",
    title: {
      text: "Successful Transaction Vol/Month",
      fontFamily: "tahoma",
      fontSize: "20",
      padding: 10
    },
    axisY: {
      title: "Volume",
      includeZero: false,
      titleFontColor: "#4F81BC",
      lineColor: "#4F81BC",
      labelFontColor: "#4F81BC",
      tickColor: "#4F81BC",
      xValueFormatString: "MMM",
    },
    axisX: {
      // title: "Successful Transaction"
      valueFormatString: "MMM"
    },
    toolTip: {
      shared: true
    },
    data: [{
      type: "column",
      yValueFormatString: "#,### Volumes",
      name: "Successful Transaction Volume",
      showInLegend: "true",
      dataPoints: dataPoints
    }]
  });

  addData(data);
}

function showValueChart(data) {

  const dataPoints = [];

  function addData(data) {

    for (var i = 0; i < data.data.value.length; i++) {
      let date = data.data.value[i][1].split(",");
      // console.log(date)
      // console.log(new Date(date[0], date[1] - 1))
      let val = data.data.value[i][0];
      dataPoints.push({
        x: new Date(date[0], date[1] - 1),
        y: val
      });
    }
    chart.render();
  }

  const chart = new CanvasJS.Chart("lineContainer", {
    animationEnabled: true,
    theme: "light1", // "light1", "light2", "dark1", "dark2"
    title: {
      text: "Successful Transaction Value/Month",
      fontFamily: "tahoma",
      fontSize: "20",
      padding: 10
    },
    axisY: {
      title: "Transaction Volume",
      includeZero: false,
      scaleBreaks: {
        autoCalculate: true
      },
      crosshair: {
        enabled: true
      },
      titleFontColor: "#4F81BC",
      lineColor: "#369EAD",
      labelFontColor: "#4F81BC",
      tickColor: "#4F81BC",
      xValueFormatString: "YYYY D MMM",
    },
    axisX: {
      title: "Month",
      lineColor: "#369EAD",
      valueFormatString: "YYYY D MMM"
    },
    toolTip: {
      shared: true
    },
    legend: {
      cursor: "pointer",
      verticalAlign: "bottom",
      horizontalAlign: "left",
      dockInsidePlotArea: true,
    },
    data: [{
      type: "line",
      color: "#F08080",
      yValueFormatString: "#,###",
      legendText: "{label}",
      name: "Transaction Value",
      dataPoints: dataPoints
    }]
  });

  addData(data);
}

