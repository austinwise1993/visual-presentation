
$.getJSON('https://cors-anywhere.herokuapp.com/178.62.33.6:8981/status/all', function (data) {

  const serviceBody = document.querySelector("#server-body");
  let str3 = "";
  let closed = false;

  data.forEach((element, index) => {

    if (index % 3 == 0) {
      if (index != 0) {
        str3 += '</div>';
        closed = true;
      }
      str3 += '<div class="row sTp" id="stat">';
    } else {
      closed = false;
    }

    let serverState = element.status.status.toLowerCase();
    let stat;

    switch (serverState) {

      case "good":
        stat = "serverUp";
        break;

      case "unknown":
        stat = "unknownServer";
        break;

      case "down":
        stat = " serverDown";
        break;

      default:
        stat = "unstableServer";
        break;
    }

    const cardValue =
      `<div class="col-4">
        <div class="row service" >
          <div class="col-2 thumbNails ">
            <img src="${element.thumbnail}" alt="MW" height="50" width="50">
          </div>
          <div class="col-9 server2">
            <div><span>${element.service}</span></div>
          </div>
          <div class="col-1 iThumb ">
            <span class=${stat} > </span>
          </div>
        </div>
      </div>`;

    str3 += cardValue;

  });

  if (!closed) {
    str3 += '</div>';
  }
  serviceBody.innerHTML = str3;

});

