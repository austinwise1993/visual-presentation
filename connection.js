
const express = require("express");
const app = express();
const port = 3000;
const bodyParser = require('body-parser');
const path = require("path");
const mongoose = require("mongoose");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

mongoose.Promise = global.Promise;
// mongoose.connect("mongodb://127.0.0.1:27017/visuals", {useNewUrlParser: true}, function(err, db) {

//     db.agents.save({"signedUpYesterday":12, "signedUpTwoDaysAgo":1713})

//     if(err) {
//         console.log("Error connecting to db");
//     } else {
//         console.log("Successfully connected to db");
//     }
// });

// var MongoClient = require('mongodb').MongoClient;
// var url = "mongodb://localhost:27017/";
// MongoClient.connect(url, function(err, db) {
//     var dbo = db.db("visuals");
//     if (err) throw err;
//     var myobj = [
//         {"signedUpYesterday":12,"signedUpTwoDaysAgo":1713},
//         {"signedUpLastMonth":789,"signedUpLastTwoMonth":1245}
//     ];

//     dbo.collection("agents").insertMany(myobj, function(err, res) {
//         if (err) throw err;
//         console.log("Number of documents inserted: " + res.insertedCount);
//         db.close();
//     });
// });

// var nameSchema = new mongoose.Schema({
//     firstName: String,
//     lastName: String
// });

console.log(path.join(__dirname, 'public/'));
app.use(express.static(path.join(__dirname, 'public/')));
app.get("/pages", (req, res) => {
    res.sendFile(__dirname + "/pages/");
});

// var User = mongoose.model("User", nameSchema);

app.post("/data", (req, res) => {
    const myData = new User(req.body);
    myData.save().then(item => {
        res.send("Saved data to database");
    }).catch(err => {
        res.status(400).send("Unable to save data to database");
    });
});

app.listen(port, () => {
    console.log("Server listening on port " + port);
});